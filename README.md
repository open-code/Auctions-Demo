This application is an incomplete offline skeleton auction app built as part of 
Crossover application process.

Features:
- User Authentication
- Items Listing
- Add Item for auction
- Add Bid of item
- Auto-bidding Bot
- Local Database storage
- Utility Class for Fake Data insertion