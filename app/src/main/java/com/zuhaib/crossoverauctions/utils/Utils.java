package com.zuhaib.crossoverauctions.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zuhaib.crossoverauctions.activities.LoginActivity;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by hp sleek book on 6/27/2015.
 */
public class Utils {

    //Show a custom alert dialog
    public static void showAlertDialogAndGoToLoginActivity(final Context c, String title, String message) {
        new AlertDialog.Builder(c).setTitle(title).setMessage(message).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Go to Login Activity
                Activity a = (Activity) c;
                a.startActivity(new Intent(c, LoginActivity.class));
                a.finish();
            }
        }).show();
    }

    public static void showAlertDialogWithoutCancel(final Context c, String title, String message) {
        new AlertDialog.Builder(c).setTitle(title).setMessage(message).setPositiveButton(android.R.string.ok, null).show();
    }


    public static void showLightSnackbar(LinearLayout layout, String message) {
        Snackbar s = Snackbar.make(layout, message, Snackbar.LENGTH_SHORT);
        TextView tv = (TextView) s.getView().findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        s.show();
    }

    public static String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public static SpannableStringBuilder getMultiStyleSignUpText(String text, int textColor, int startIndext, int endIndex, boolean isBold, boolean isItalic) {
        SpannableStringBuilder sb = new SpannableStringBuilder(text);
        // Span to set text color to some RGB value
        ForegroundColorSpan fcs = new ForegroundColorSpan(textColor);
        StyleSpan bss;
        if(isBold && isItalic) {
            // Span to make text bold and italic
            bss = new StyleSpan(Typeface.BOLD_ITALIC);
        }else if(isBold) {
            // Span to make text bold
            bss = new StyleSpan(Typeface.BOLD);
        }else if(isItalic) {
            // Span to make text bold
            bss = new StyleSpan(Typeface.ITALIC);
        }else{
            bss = new StyleSpan(Typeface.BOLD);
        }
        // Set the text color for first 4 characters
        sb.setSpan(fcs, startIndext, endIndex, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        // make them also bold
        sb.setSpan(bss, startIndext, endIndex, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        return sb;
    }

    /**
     * get datetime
     * */
    public static String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static boolean isValidEmail(String email) {
        if (TextUtils.isEmpty(email)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }
    }

    public static Bitmap blobToBitmap(byte[] blob) {
        return (blob != null) ? BitmapFactory.decodeByteArray(blob, 0 ,blob.length) : null;
    }

    public static byte[] bitmapToBlob(Bitmap bitmap){
        if(bitmap != null) {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
            return bos.toByteArray();
        }
        return null;
    }

    public static String capitalizeFirstLetter(String word) {
        return word.substring(0,1).toUpperCase() + word.substring(1).toLowerCase();
    }
}
