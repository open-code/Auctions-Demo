package com.zuhaib.crossoverauctions.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.zuhaib.crossoverauctions.Globals;
import com.zuhaib.crossoverauctions.R;
import com.zuhaib.crossoverauctions.activities.dialogs.NewBidDialog;
import com.zuhaib.crossoverauctions.database.AuctionsDatabase;
import com.zuhaib.crossoverauctions.models.Item;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Zuhaib on 1/2/2016.
 */

public class AuctionAdapter extends RecyclerView.Adapter<AuctionAdapter.AuctionItemViewHolder> implements View.OnClickListener {

    private FragmentManager fm;
    private List<Item> items;
    private int id;
    private AuctionsDatabase database;
    private Context c;

    public AuctionAdapter(List<Item> items, FragmentManager fm) {
        this.items = items;
        this.fm = fm;
    }

    @Override
    public AuctionItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Get Context
        c = parent.getContext();
        //Instantiate database
        database = new AuctionsDatabase(c);
        //Inflate layout
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.auction_item, parent, false);
        AuctionItemViewHolder viewHolder = new AuctionItemViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AuctionItemViewHolder holder, int position) {
        id = items.get(position).getId();
        getNumberOfBids(position);
        holder.bidButton.setOnClickListener(this);
        Bitmap b = items.get(position).getImage();
        if(b != null) holder.itemImageView.setImageBitmap(b);
        else holder.itemImageView.setImageBitmap(BitmapFactory.decodeResource(c.getResources(),R.drawable.gramophone));
        holder.openTimeTextView.setText("Auction Started:" + items.get(position).getOpenTime());
        holder.closeTImeTextView.setText("Auction Closes:" + items.get(position).getCloseTime());
        holder.bidsTextView.setText(items.get(position).getNumberOfBids() + " Bids");
        holder.priceTextView.setText(items.get(position).getPrice() + " USD");
        holder.nameTextView.setText(Utils.capitalizeFirstLetter(items.get(position).getName()));
        holder.donorTextView.setText(Utils.capitalizeFirstLetter(items.get(position).getDonorName()));
        holder.descriptionTextView.setText(Utils.capitalizeFirstLetter(items.get(position).getDescription()));
    }

    private void getNumberOfBids(int position) {
        try {
            database.open();
            items.get(position).setNumberOfBids(database.getNumberOfBids(id));
            database.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if(id == R.id.BTBid){
            showBidDialog();
        }
    }

    private void showBidDialog() {
        NewBidDialog d = new NewBidDialog();
        Bundle b = new Bundle();
        b.putInt(Globals.EXTRA_ITEM_ID, id);
        d.setArguments(b);
        d.show(fm,".dialog_bid");
    }

    class AuctionItemViewHolder extends RecyclerView.ViewHolder{

        CardView cardView;
        ImageView itemImageView;
        TextView openTimeTextView;
        TextView closeTImeTextView;
        TextView bidsTextView;
        TextView priceTextView;
        TextView nameTextView;
        TextView donorTextView;
        TextView descriptionTextView;
        Button bidButton;

        public AuctionItemViewHolder(View itemView) {
            super(itemView);

            cardView = (CardView) itemView.findViewById(R.id.cv);
            itemImageView = (ImageView) itemView.findViewById(R.id.IVItemImage);
            openTimeTextView = (TextView) itemView.findViewById(R.id.TVOpenTime);
            closeTImeTextView = (TextView) itemView.findViewById(R.id.TVCloseTime);
            bidsTextView = (TextView) itemView.findViewById(R.id.TVBids);
            priceTextView = (TextView) itemView.findViewById(R.id.TVPrice);
            nameTextView = (TextView) itemView.findViewById(R.id.TVName);
            donorTextView = (TextView) itemView.findViewById(R.id.TVDonor);
            descriptionTextView = (TextView) itemView.findViewById(R.id.TVDescription);
            bidButton = (Button) itemView.findViewById(R.id.BTBid);
        }
    }
}
