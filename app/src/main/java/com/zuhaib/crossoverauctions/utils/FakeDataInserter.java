package com.zuhaib.crossoverauctions.utils;

import android.content.Context;

import com.zuhaib.crossoverauctions.R;
import com.zuhaib.crossoverauctions.activities.ItemsListActivity;
import com.zuhaib.crossoverauctions.models.Bid;
import com.zuhaib.crossoverauctions.models.Item;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Zuhaib on 1/3/2016.
 */
public class FakeDataInserter {

    private Random r;
    ArrayList<Item> items;
    Context c;

    public FakeDataInserter(Context c) {
        r = new Random();
        this.c = c;
    }

    public void insertFakeItems(int qty){
        //Get Item List Activity instance from context
        ItemsListActivity activity = (ItemsListActivity) c;
        //Loop through 'qty' times
        for(int i = 0 ; i < qty ; i++){
            //Make every 3rd items have no image
            int resourceId = (i % 3 == 0) ? R.drawable.default_auction_item_image : R.drawable.gramophone;
            //Insert items
            activity.insertNewAuctionItem(
                    "Auction Item - " + (i+1),
                    "This is dummy auction item number " + (i+1),
                    "30-12-2015",
                    "01-01-2016",
                    String.valueOf(r.nextInt(1000 - 50) + 50),
                    null
                    //BitmapFactory.decodeResource(activity.getResources(), resourceId)
            );
        }
    }

    public void placeRandomBidsOnItems(){
        //Get Item List Activity instance from context
        ItemsListActivity activity = (ItemsListActivity) c;

        // Fetch Auction Items from Database
        items = activity.getAuctionItemsFromDatabase();

        //Insert Random Number of bids with Random values on each Item
        for(Item i : items){
            int noOfBids = r.nextInt(15 - 0) + 0;
            for(int j = 0 ; j < noOfBids ; j++){
                int randomUserID = r.nextInt(10 - 1) + 1;
                // Bid as the user on every 3rd item to make sure some items are in Bidding category
                if(j % 3 == 0) randomUserID = 2;
                activity.insertNewBidOnItem(i.getId(), r.nextInt(1000 - 50) + 50, randomUserID, "Random User No "+randomUserID);
            }
        }
    }

    public void makeSomeOfMyBidsWin(){
        //Get Item List Activity instance from context
        ItemsListActivity activity = (ItemsListActivity) c;

        // Fetch Auction Items from Database
        ArrayList<Bid> bids = activity.getMyBids();

        // Of all bids, make some winner
        for(Bid b : bids){
            // Make bid win if random number is even
            boolean isWinning = (r.nextInt() % 2 == 0) ? true : false;
            for(Item i : items){
                //If is winner and item has this bid
                if(b.getItemID() == i.getId() && isWinning){
                    activity.setBidWinner(b.getId(), i.getId());
                }
            }
        }
    }
}
