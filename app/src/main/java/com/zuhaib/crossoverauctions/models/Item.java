package com.zuhaib.crossoverauctions.models;

import android.graphics.Bitmap;

/**
 * Created by Zuhaib on 1/1/2016.
 */
public class Item {
    private int id;
    private int donorID;
    private String name;
    private String description;
    private String donorName;
    private Bitmap image;
    private int price;
    private String openTime;
    private String closeTime;
    private int numberOfBids;
    private int acceptedID;

    public Item(int id, int donorID, String name, String description, String donorName, Bitmap image, int price, String openTime, String closeTime, int acceptedID) {
        this.id = id;
        this.donorID = donorID;
        this.name = name;
        this.description = description;
        this.donorName = donorName;
        this.image = image;
        this.price = price;
        this.openTime = openTime;
        this.closeTime = closeTime;
        this.acceptedID = acceptedID;
    }

    public int getAcceptedID() {
        return acceptedID;
    }

    public int getId() {
        return id;
    }

    public int getDonorID() {
        return donorID;
    }

    public void setDonorID(int donorID) {
        this.donorID = donorID;
    }

    public void setNumberOfBids(int bids){
        this.numberOfBids = bids;
    }

    public int getNumberOfBids() {
        return numberOfBids;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDonorName() {
        return donorName;
    }

    public void setDonorName(String donorName) {
        this.donorName = donorName;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int startingPrice) {
        this.price = startingPrice;
    }

    public String getOpenTime() {
        return openTime;
    }

    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    public String getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(String closeTime) {
        this.closeTime = closeTime;
    }

    @Override
    public String toString() {
        return "\nItem{" +
                "id=" + id +
                ", donorID=" + donorID +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", donorName='" + donorName + '\'' +
                ", image=" + image +
                ", price=" + price +
                ", openTime='" + openTime + '\'' +
                ", closeTime='" + closeTime + '\'' +
                ", numberOfBids=" + numberOfBids +
                ", acceptedID=" + acceptedID +
                '}';
    }
}
