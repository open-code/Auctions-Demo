package com.zuhaib.crossoverauctions.models;

/**
 * Created by Zuhaib on 1/1/2016.
 */
public class Bid {

    private int id;
    private int amount;
    private String bidderName;
    private int itemID;
    private int bidderID;

    public Bid(int id, int amount, String bidderName, int itemID, int bidderID) {
        this.id = id;
        this.amount = amount;
        this.bidderName = bidderName;
        this.itemID = itemID;
        this.bidderID = bidderID;
    }

    public int getId() {
        return id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getBidderName() {
        return bidderName;
    }

    public void setBidderName(String bidderName) {
        this.bidderName = bidderName;
    }

    public int getItemID() {
        return itemID;
    }

    public void setItemID(int itemID) {
        this.itemID = itemID;
    }

    public int getBidderID() {
        return bidderID;
    }

    public void setBidderID(int bidderID) {
        this.bidderID = bidderID;
    }
}
