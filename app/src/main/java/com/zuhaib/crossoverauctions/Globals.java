package com.zuhaib.crossoverauctions;

import android.app.Application;
import android.content.Context;

/**
 * Created by Zuhaib on 12/31/2015.
 */
public class Globals extends Application{
    public static final String TAG = "com.zuhaib.auctions";
    public static final String SHARED_PREFS_NAME = "auctions_app";

    public static final String EXTRA_USERNAME = "com.zuhaib.auctions.username";
    public static final String EXTRA_USER_ID = "com.zuhaib.auctions.userID";
    public static final String EXTRA_IS_LOGIN = "com.zuhaib.auctions.is_login";
    public static final String EXTRA_IS_FIRST_USE = "com.zuhaib.auctions.is_first_use";
    public static final String EXTRA_IS_ADMIN = "com.zuhaib.auctions.is_admin";
    public static final String EXTRA_IS_FAKE_DATA_NEEDED = "com.zuhaib.auctions.is_fake_data_needed";
    public static final String EXTRA_ITEM_ID = "com.zuhaib.auctions.item_id";

    /**
     * ONLY THOSE VARIABLES ARE PLACED HERE WHICH NEEDS GLOBAL ACCESS THROUGHOUT APPLICATION LIFECYCLE (e.g. User ID, Login Status, User Name etc)
     * */

    public void logout() {
        getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE).edit().putBoolean(Globals.EXTRA_IS_LOGIN, false).commit();
    }

    public void login(){
        getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE).edit().putBoolean(Globals.EXTRA_IS_LOGIN, true).commit();
    }

    public boolean isLogin(){
        return getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE).getBoolean(Globals.EXTRA_IS_LOGIN, false);
    }

    public boolean isFirstUse() {
        return getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE).getBoolean(Globals.EXTRA_IS_FIRST_USE, true);
    }

    public void setIsNotFirstUse(){
        getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE).edit().putBoolean(Globals.EXTRA_IS_FIRST_USE, false).commit();
    }

    public int getUserID() {
        return getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE).getInt(Globals.EXTRA_USER_ID, 0);
    }
    
    public void setUserID(int userID){
        getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE).edit().putInt(Globals.EXTRA_USER_ID, userID).commit();
    }
    
    public String getUsername(){
        return getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE).getString(Globals.EXTRA_USERNAME, "");
    }
    
    public void setUsername(String username){
        getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE).edit().putString(Globals.EXTRA_USERNAME, username).commit();
    }

    public void setIsAdmin(boolean isAdmin){
        getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE).edit().putBoolean(Globals.EXTRA_IS_ADMIN, isAdmin).commit();
    }

    public boolean isAdmin(){
        return getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE).getBoolean(Globals.EXTRA_IS_ADMIN, false);
    }

    public void setIsFakeDataNeeeded(boolean isNeeded){
        getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE).edit().putBoolean(Globals.EXTRA_IS_FAKE_DATA_NEEDED, isNeeded).commit();
    }

    public boolean isFakeDataNeeded(){
        return getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE).getBoolean(Globals.EXTRA_IS_FAKE_DATA_NEEDED, true);
    }
}
