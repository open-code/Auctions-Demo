package com.zuhaib.crossoverauctions.activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.zuhaib.crossoverauctions.Globals;
import com.zuhaib.crossoverauctions.R;
import com.zuhaib.crossoverauctions.database.AuctionsDatabase;
import com.zuhaib.crossoverauctions.models.User;
import com.zuhaib.crossoverauctions.utils.Utils;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    //============================================================================================//
    //                                      UI References                                         //
    //============================================================================================//
    private View mProgressView;
    private EditText usernameEditText, passwordEditText, usernameRegisterEditText, passwordRegisterEditText, dobRegisterEditText, emailRegisterEditText;
    private Button loginButton, signUpButton;
    private ImageView logoImageView;
    private LinearLayout container, loginForm, signupForm;
    private TextView signupTextView, loginTextView;

    //============================================================================================//
    //                                  References Type Variables                                 //
    //============================================================================================//
    private AuctionsDatabase database;
    private Globals app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // remove title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        if (preLaunchActivities()) return;
        setContentView(R.layout.activity_login);

        //Setup UI components
        SetupUI();

        //Get database
        database = new AuctionsDatabase(this);

        //Add necessary items to database on first use
        AddFirstUseItemsToDatabase();
    }

    private boolean preLaunchActivities() {

        //Initialize Global context
        app = (Globals) getApplication();

        //If user already logged in
        if (app.isLogin()) {
            //Save User ID
            //If no ID then force Re-Login
            if (app.getUserID() <= 0) {
                Utils.showAlertDialogWithoutCancel(this, "Login Error",
                        "Invalid or No User ID Found, Please Login Again To Continue Using Globals");
                return true;
            }
            //Else Proceed to main menu
            startActivity(new Intent(LoginActivity.this, ItemsListActivity.class));
            finish();
        }
        return false;
    }

    private void AddFirstUseItemsToDatabase() {
        if(app.isFirstUse()){
            //Automatically add a Admin in the Users table
            attemptRegister("admin", "abc@yahoo.com", "admin", "2012-12-12");
            attemptRegister("zuhaib", "abc@yahoo.com", "admin", "2012-12-12");
            app.setIsNotFirstUse();
        }
    }

    private void SetupUI() {

        //References to Login and Register form items
        container = (LinearLayout) findViewById(R.id.LLContainer);
        loginForm = (LinearLayout) findViewById(R.id.LLLoginForm);
        signupForm = (LinearLayout) findViewById(R.id.LLRegisterForm);
        mProgressView = findViewById(R.id.login_progress);
        usernameEditText = (EditText) findViewById(R.id.ETUsername);
        passwordEditText = (EditText) findViewById(R.id.ETPassword);
        usernameRegisterEditText = (EditText) findViewById(R.id.ETRUsername);
        passwordRegisterEditText = (EditText) findViewById(R.id.ETRPassword);
        dobRegisterEditText = (EditText) findViewById(R.id.ETRDOB);
        emailRegisterEditText = (EditText) findViewById(R.id.ETREmail);
        loginButton = (Button) findViewById(R.id.BLogIn);
        signUpButton = (Button) findViewById(R.id.BTRSingUP);
        signupTextView = (TextView) findViewById(R.id.TVSignUp);
        loginTextView = (TextView) findViewById(R.id.TVLogin);

        //Make DOB EditText not receive inputs itself so a DatePickerDialog can be utilized
        dobRegisterEditText.setInputType(InputType.TYPE_NULL);
        dobRegisterEditText.requestFocus();

        //Get Special Spannable text for Sign Up and Login texts
        signupTextView.setText(Utils.getMultiStyleSignUpText("Don't Have Account Yet?\nSign Up Now!", Color.WHITE, 23, 31, true, true));
        loginTextView.setText(Utils.getMultiStyleSignUpText("Already Have An Account?\nLog In Now!", Color.WHITE, 25, 31, true, true));

        //Load Username from global variables and if found, place to username EditText; to prevent redundancy of adding username every time by the user
        String username = app.getUsername();
        usernameEditText.setText(username);

        //On Click listeners
        usernameEditText.setOnClickListener(this);
        passwordEditText.setOnClickListener(this);
        usernameRegisterEditText.setOnClickListener(this);
        passwordRegisterEditText.setOnClickListener(this);
        dobRegisterEditText.setOnClickListener(this);
        emailRegisterEditText.setOnClickListener(this);
        signupTextView.setOnClickListener(this);
        loginTextView.setOnClickListener(this);
        loginButton.setOnClickListener(this);
        signUpButton.setOnClickListener(this);

    }

    /**
     * Attempts to sign in the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    public void attemptLogin(final String username, final String password) {
        //Hide Progressbar
        showProgress(false);

        User u;

        // Invoke Database's Login method which queries for matching Username/Password pair in Users database
        // and return a @User instance if successful
        try {
            database.open();
            u = database.attemptUserLogin(username, password);
            database.close();
        } catch (SQLException e) {
            e.printStackTrace();
            Utils.showAlertDialogWithoutCancel(this, "Error", "Error Occurred While Access Database");
            return;
        }

        if (u != null) {
            if (u.getUsername() != null) {
                //Greet User
                Toast.makeText(this, "Welcome. " + u.getUsername() + "!", Toast.LENGTH_SHORT).show();

                // If username is different from previous value, Save the new Username
                if (!u.getUsername().equals(app.getUsername())) app.setUsername(u.getUsername());

                // Save User ID
                if (u.getUserId() >= 0) app.setUserID(u.getUserId());

                //Save User Login Status
                app.login();

                //Put admin flag if admin detected (For simplicity, Admin is assigned both Username and Password as admin and that no one else can have)
                if(u.getUsername().equalsIgnoreCase("admin") && u.getPassword().equalsIgnoreCase("admin")) app.setIsAdmin(true);

                // Navigate to AuctionsItemsActivity
                startActivity(new Intent(LoginActivity.this, ItemsListActivity.class));
                finish();
            }
        } else {
            Utils.showAlertDialogWithoutCancel(this, "Error", "Login Unsuccessful! Please Provide A Valid Username/Password Combination");
        }
    }

    /**
     * Attempts to sign up or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    public void attemptRegister(final String username, final String email, final String password, final String dob) {
        //Hide Progressbar
        showProgress(false);

        boolean success;

        // Invoke Database's Register method which attempts to insert new item in the Users database
        // and return a @User instance if successful
        try {
            database.open();
            success = database.attemptUserRegister(username, email, password, dob);
            database.close();
        } catch (SQLException e) {
            e.printStackTrace();
            Utils.showAlertDialogWithoutCancel(this, "Error", "Error Occurred While Access Database");
            return;
        }

        if (success) {
            //Show Login Form
            showLoginForm();

            // Prompt Registration success
            Utils.showLightSnackbar(container, "User Has Been Registered Successfully!");
        } else {
            Utils.showAlertDialogWithoutCancel(this, "Error", "Unfortunately! An Error Occurred While Registering The User. Please Try Again!");
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    public void showProgress(final boolean show) {
        // The ViewPropertyAnimator APIs are not available, so simply show
        // and hide the relevant UI components.
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.BLogIn) {
            showProgress(true);
            String username = usernameEditText.getText().toString();
            String password = passwordEditText.getText().toString();

            // Validations
            if (validateLoginForm(username, password)) return;

            // Login Operation
            attemptLogin(username, password);
        } else if (id == R.id.BTRSingUP) {
            showProgress(true);
            String username = usernameRegisterEditText.getText().toString();
            String password = passwordRegisterEditText.getText().toString();
            String dob = dobRegisterEditText.getText().toString();
            String email = emailRegisterEditText.getText().toString();

            // Validations
            if (validateRegistrationForm(username, password, dob, email)) return;

            // Registration Operation
            attemptRegister(username, email, password, dob);
        } else if (id == R.id.ETUsername || id == R.id.ETPassword || id == R.id.ETREmail || id == R.id.ETRPassword || id == R.id.ETRUsername) {
            // Hide progressbar if progress view is being displayed and user clicks one of these views
            showProgress(false);
        } else if (id == R.id.TVSignUp) {
            //Switch Forms and Hide Sign Up Text
            signupTextView.setVisibility(View.GONE);
            loginTextView.setVisibility(View.VISIBLE);
            loginForm.setVisibility(View.GONE);
            signupForm.setVisibility(View.VISIBLE);
        } else if (id == R.id.TVLogin) {
            showLoginForm();
        } else if (id == R.id.ETRDOB) {
            Calendar newCalendar = Calendar.getInstance();
            DatePickerDialog d = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(year, monthOfYear, dayOfMonth);
                    SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                    dobRegisterEditText.setText(dateFormatter.format(newDate.getTime()));
                }
            }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
            d.show();
        }
    }

    private boolean validateLoginForm(String username, String password) {
        if (username.isEmpty()) {
            usernameEditText.setError("Field Can't Be Left Empty");
            showProgress(false);
            return true;
        }
        if (password.isEmpty()) {
            passwordEditText.setError("Field Can't Be Left Empty");
            showProgress(false);
            return true;
        }
        if (username.contains(" ")) {
            usernameEditText.setError("Username Can't Have Spaces");
            showProgress(false);
            return true;
        }
        if (password.contains(" ")) {
            passwordEditText.setError("Password Can't Have Spaces");
            showProgress(false);
            return true;
        }
        return false;
    }

    private boolean validateRegistrationForm(String username, String password, String dob, String email) {
        if (username.equalsIgnoreCase(password)) {
            usernameRegisterEditText.setError("Password Can't Be Same As Username");
            showProgress(false);
            return true;
        }
        if (username.isEmpty()) {
            usernameRegisterEditText.setError("Field Can't Be Left Empty");
            showProgress(false);
            return true;
        }
        if (password.isEmpty()) {
            passwordRegisterEditText.setError("Field Can't Be Left Empty");
            showProgress(false);
            return true;
        }
        if (dob.isEmpty()) {
            dobRegisterEditText.setError("Field Can't Be Left Empty");
            showProgress(false);
            return true;
        }
        if (email.isEmpty()) {
            emailRegisterEditText.setError("Field Can't Be Left Empty");
            showProgress(false);
            return true;
        }
        if (!Utils.isValidEmail(email)) {
            emailRegisterEditText.setError("Please Provide A Valid Email");
            showProgress(false);
            return true;
        }
        if (username.contains(" ")) {
            usernameRegisterEditText.setError("Username Can't Have Spaces");
            showProgress(false);
            return true;
        }
        if (password.contains(" ")) {
            passwordRegisterEditText.setError("Password Can't Have Spaces");
            showProgress(false);
            return true;
        }
        if (email.contains(" ")) {
            emailRegisterEditText.setError("Email Can't Have Spaces");
            showProgress(false);
            return true;
        }
        return false;
    }

    private void showLoginForm() {
        //Switch Forms and Hide Sign Up Text
        signupTextView.setVisibility(View.VISIBLE);
        loginTextView.setVisibility(View.GONE);
        loginForm.setVisibility(View.VISIBLE);
        signupForm.setVisibility(View.GONE);
    }
}

