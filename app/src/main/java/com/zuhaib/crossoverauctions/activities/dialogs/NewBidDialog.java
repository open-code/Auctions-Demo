package com.zuhaib.crossoverauctions.activities.dialogs;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.zuhaib.crossoverauctions.Globals;
import com.zuhaib.crossoverauctions.R;
import com.zuhaib.crossoverauctions.activities.ItemsListActivity;

/**
 * Created by Zuhaib on 1/3/2016.
 */
public class NewBidDialog extends AppCompatDialogFragment implements View.OnClickListener {

    //============================================================================================//
    //                                      UI References                                         //
    //============================================================================================//
    private EditText amountEditText;
    private Button bidButton, cancelButton;

    //============================================================================================//
    //                                  References Type Variables                                 //
    //============================================================================================//
    private OnBidAddedCallback listener;

    //============================================================================================//
    //                                      Type Variables                                        //
    //============================================================================================//
    int itemID;

    public interface OnBidAddedCallback {
        void onAdded();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener = (OnBidAddedCallback) activity;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_new_bid, null);
        setCancelable(false);

        itemID = getArguments().getInt(Globals.EXTRA_ITEM_ID);

        amountEditText = (EditText) v.findViewById(R.id.ETAmount);
        cancelButton = (Button) v.findViewById(R.id.BTCancel);
        bidButton = (Button) v.findViewById(R.id.BTSubmit);
        bidButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.BTCancel) {
            dismiss();
        } else if (id == R.id.BTSubmit) {
            String amount = amountEditText.getText().toString();
            if (amount.isEmpty()) {
                amountEditText.setError("Field Can't Be Left Empty");
                return;
            }
            //Insert into database
            ((ItemsListActivity) getActivity()).insertNewBidOnItem(itemID, Integer.parseInt(amount));
            listener.onAdded();
            this.dismiss();
        }
    }


}
