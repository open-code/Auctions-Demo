package com.zuhaib.crossoverauctions.activities.dialogs;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.zuhaib.crossoverauctions.R;
import com.zuhaib.crossoverauctions.activities.ItemsListActivity;
import com.zuhaib.crossoverauctions.utils.Utils;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Zuhaib on 1/3/2016.
 */
public class NewAuctionItemDialog extends DialogFragment implements View.OnClickListener {

    private static final int PICK_PHOTO_FOR_AVATAR = 1;

    //============================================================================================//
    //                                      UI References                                         //
    //============================================================================================//
    private EditText nameEditText, descriptionEditText, openDateEditText, closeDateEditText, priceEditText;
    private ImageView photoImageView;
    private Button photoButton, cancelButton, publishButton;
    private Bitmap image = null;

    //============================================================================================//
    //                                  References Type Variables                                 //
    //============================================================================================//
    private Uri ImageURI;
    private OnItemAddedCallback listener;


    //============================================================================================//
    //                                      Type Variables                                        //
    //============================================================================================//
    private String filename;


    public interface OnItemAddedCallback {
        void onAdded();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener = (OnItemAddedCallback) activity;
    }

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {


        // the content
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(root);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_new_item, null);
        setCancelable(false);

        nameEditText = (EditText) v.findViewById(R.id.ETName);
        descriptionEditText = (EditText) v.findViewById(R.id.ETDescription);
        openDateEditText = (EditText) v.findViewById(R.id.ETOpenDate);
        closeDateEditText = (EditText) v.findViewById(R.id.ETCloseDate);
        priceEditText = (EditText) v.findViewById(R.id.ETStartingPrice);
        photoImageView = (ImageView) v.findViewById(R.id.IVPhoto);
        photoImageView.setVisibility(View.GONE);
        photoButton = (Button) v.findViewById(R.id.BTImage);
        cancelButton = (Button) v.findViewById(R.id.BTCancel);
        publishButton = (Button) v.findViewById(R.id.BTPublish);

        photoButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);
        publishButton.setOnClickListener(this);
        openDateEditText.setOnClickListener(this);
        closeDateEditText.setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.BTCancel) {
            dismiss();
        } else if (id == R.id.BTPublish) {
            String name = nameEditText.getText().toString();
            if (name.isEmpty()) {
                nameEditText.setError("Field Can't Be Left Empty");
                return;
            }
            String description = descriptionEditText.getText().toString();
            if (description.isEmpty()) {
                descriptionEditText.setError("Field Can't Be Left Empty");
                return;
            }
            String openDate = openDateEditText.getText().toString();
            if (openDate.isEmpty()) {
                openDateEditText.setError("Field Can't Be Left Empty");
                return;
            }
            String closeDate = closeDateEditText.getText().toString();
            if (closeDate.isEmpty()) {
                closeDateEditText.setError("Field Can't Be Left Empty");
                return;
            }
            String price = priceEditText.getText().toString();
            if (price.isEmpty()) {
                nameEditText.setError("Field Can't Be Left Empty");
                return;
            }
            if (photoImageView.getDrawable() != null) {
                image = ((BitmapDrawable) photoImageView.getDrawable()).getBitmap();
            }

            //Insert into database
            ((ItemsListActivity) getActivity()).insertNewAuctionItem(name, description, openDate, closeDate, price, image);
            listener.onAdded();
            this.dismiss();

        } else if (id == R.id.BTImage) {
            openGallery();
        } else if(id == R.id.ETOpenDate){
            Calendar newCalendar = Calendar.getInstance();
            DatePickerDialog d = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(year, monthOfYear, dayOfMonth);
                    SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                    openDateEditText.setText(dateFormatter.format(newDate.getTime()));
                }
            }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
            d.show();
        } else if(id == R.id.ETCloseDate){
            Calendar newCalendar = Calendar.getInstance();
            DatePickerDialog d = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(year, monthOfYear, dayOfMonth);
                    SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                    closeDateEditText.setText(dateFormatter.format(newDate.getTime()));
                }
            }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
            d.show();
        }
    }

    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, PICK_PHOTO_FOR_AVATAR);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_PHOTO_FOR_AVATAR && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                //Display an error
                return;
            }
            try {
                ImageURI = data.getData();
                filename = Utils.getRealPathFromURI(getActivity(), ImageURI);
                filename = filename.substring(filename.lastIndexOf("/") + 1);
                filename = filename.replace(" ", "");
                InputStream inputStream = getActivity().getContentResolver().openInputStream(data.getData());
                Bitmap b = BitmapFactory.decodeStream(inputStream);
                if (b != null) {
                    photoImageView.setVisibility(View.VISIBLE);
                    photoImageView.setImageBitmap(b);
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
