package com.zuhaib.crossoverauctions.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.zuhaib.crossoverauctions.Bot.AutoBiddingBot;
import com.zuhaib.crossoverauctions.Globals;
import com.zuhaib.crossoverauctions.R;
import com.zuhaib.crossoverauctions.activities.dialogs.NewAuctionItemDialog;
import com.zuhaib.crossoverauctions.activities.dialogs.NewBidDialog;
import com.zuhaib.crossoverauctions.database.AuctionsDatabase;
import com.zuhaib.crossoverauctions.models.Bid;
import com.zuhaib.crossoverauctions.models.Item;
import com.zuhaib.crossoverauctions.utils.AuctionAdapter;
import com.zuhaib.crossoverauctions.utils.FakeDataInserter;
import com.zuhaib.crossoverauctions.utils.Utils;

import java.sql.SQLException;
import java.util.ArrayList;

public class ItemsListActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, NewAuctionItemDialog.OnItemAddedCallback, NewBidDialog.OnBidAddedCallback {

    //============================================================================================//
    //                                      UI References                                         //
    //============================================================================================//
    private RecyclerView auctionItemsRecyclerView;

    //============================================================================================//
    //                                  References Type Variables                                 //
    //============================================================================================//
    private Globals app;
    private AuctionsDatabase database;
    private LinearLayoutManager layoutManager;
    private FragmentManager fm;
    private AuctionAdapter adapter;

    //============================================================================================//
    //                                      Type Variables                                        //
    //============================================================================================//
    private ArrayList<Item> items;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Check for prelaunch conditions
        if (preLaunchActivities()) return;

        setContentView(R.layout.activity_items_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Setup Navigation Drawer and Floating Action Button
        prepareDrawerAndFloatingButton(toolbar);

        //Setup UI components
        SetupUI();

        //Setup the database
        database = new AuctionsDatabase(this);

        //Get support fragment manager
        fm = getSupportFragmentManager();

        //Add dummy data if needed
        addDummyData();

        //Fetch Auction items from database and fill RecyclerView with them
        addAuctionItemsToRecyclerView();

    }

    //Instantiates FakeDataInserter and inserts, bids and mark as won the dummy items
    private void addDummyData() {
        if(app.isFakeDataNeeded()){
            //Add 10 dummy items
            FakeDataInserter fake = new FakeDataInserter(this);
            fake.insertFakeItems(10);

            //Place random bids on these items
            fake.placeRandomBidsOnItems();

            //Make few randomly selected bids of user as winner
            fake.makeSomeOfMyBidsWin();

            //Set to false so next time another 10 items won't be inserted into database
            app.setIsFakeDataNeeeded(false);
        }
    }

    //Checks for valid user ID to confirm a legit login
    private boolean preLaunchActivities() {
        //Initialize Global context
        app = (Globals) getApplication();

        if(app.getUserID() <= 0){
            //Logout
            app.logout();

            //Go To Login Activity
            Utils.showAlertDialogAndGoToLoginActivity(this, "Error", "Invalid or No User ID found. Please Try Signing In Again.");
            return true;
        }
        return false;
    }

    //Fetches all auction items from database and adds them to Recycler View
    public void addAuctionItemsToRecyclerView() {
        // Fetch Auction Items from Database
        items = getAuctionItemsFromDatabase();

        //Fill Recycler view with items fetched from database
        adapter = new AuctionAdapter(items, fm);
        auctionItemsRecyclerView.setAdapter(adapter);
    }

    //Fetches bidding items from database and adds them to Recycler View
    private void addItemsBiddingToRecyclerView() {
        // Fetch Items Bidding from Database
        items = getItemsBiddingFromDatabase(app.getUserID());

        //Fill Recycler view with items fetched from database
        adapter = new AuctionAdapter(items,fm);
        auctionItemsRecyclerView.setAdapter(adapter);
    }

    //Fetches items won from database and adds them to Recycler View
    private void addItemsWonToRecyclerView() {
        // Fetch Items Won from Database
        items = getItemsWonFromDatabase(app.getUserID());

        //Fill Recycler view with items fetched from database
        adapter = new AuctionAdapter(items,fm);
        auctionItemsRecyclerView.setAdapter(adapter);
    }

    //Handles all the UI setup work
    private void SetupUI() {
        auctionItemsRecyclerView = (RecyclerView) findViewById(R.id.RVItems);
        layoutManager = new LinearLayoutManager(this);
        auctionItemsRecyclerView.setLayoutManager(layoutManager);
    }

    //Prepares Navigation drawer and Floating button
    private void prepareDrawerAndFloatingButton(Toolbar toolbar) {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showNewAuctionItemDialog();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    //Display dialog to add new auction item
    private void showNewAuctionItemDialog() {
        new NewAuctionItemDialog().show(fm, ".NewAuctionItemDialog");
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.MIAllItems) {
            addAuctionItemsToRecyclerView();
        } else if (id == R.id.MIItemsBidding) {
            addItemsBiddingToRecyclerView();
        } else if (id == R.id.MIItemsWon) {
            addItemsWonToRecyclerView();
        } else if (id == R.id.MIStartBot) {
            new AutoBiddingBot(this).start();
            //Refresh list
            refresh();
        } else if (id == R.id.MILogOut) {
            //Set Login Status to Log Out
            app.logout();
            //Go back to Login Activity
            gotoLogin();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //Navigate to login activity
    private void gotoLogin() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    //Handles database manipulation and error handling for fetching auction items
    public ArrayList<Item> getAuctionItemsFromDatabase() {
        setTitle("Auction Items");
        ArrayList<Item> list = null;
        try {
            database.open();
            list = database.getAllAuctionItems();
            Log.e(Globals.TAG, "ALL AUCTION ITEMS: "+list.toString());
            database.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    //Handles database manipulation and error handling for fetching bidding items
    public ArrayList<Item> getItemsBiddingFromDatabase(int userID) {
        setTitle("Items Bidding");
        ArrayList<Item> list = null;
        try {
            database.open();
            list = database.getAllItemsBidding(userID);
            Log.e(Globals.TAG, "BIDDING AUCTION ITEMS: "+list.toString());
            database.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    //Handles database manipulation and error handling for fetching items won
    public ArrayList<Item> getItemsWonFromDatabase(int userID) {
        setTitle("Items Won");
        ArrayList<Item> list = null;
        try {
            database.open();
            list = database.getAllItemsWon(userID);
            Log.e(Globals.TAG, "ITEMS WON: "+list.toString());
            database.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    //Handles database manipulation and error handling for inserting new auction item to database
    public void insertNewAuctionItem(String name, String description, String openDate, String closeDate, String price, Bitmap image){

        boolean success;
        // Invoke Database's Insert New Auction Item method which attempts to insert new auction item in the database
        // and return an Item instance if successful
        try {
            database.open();
            success = database.addNewAuctionItem(app.getUserID(), name, description, app.getUsername(), image,
                    Integer.parseInt(price), openDate, closeDate);
            database.close();
        } catch (SQLException e) {
            e.printStackTrace();
            Utils.showAlertDialogWithoutCancel(this, "Error", "Error Occurred While Access Database");
            return;
        }
        //Refresh list if successful
        if (success)
            refresh();
        else
            Utils.showAlertDialogWithoutCancel(this, "Error", "Unfortunately! An Error Occurred While Inserting Item In The Database. Please Try Again!");
    }

    private void refresh() {
        if(adapter != null)
            adapter.notifyDataSetChanged();
    }

    //Handles database manipulation and error handling for inserting new bid on items
    public void insertNewBidOnItem(int itemID, int amount){

        boolean success;
        // Invoke Database's Insert New Auction Item method which attempts to insert new auction item in the database
        // and return an Item instance if successful
        try {
            database.open();
            success = database.addNewBidOnItem(amount, app.getUserID(), app.getUsername(), itemID);
            database.close();
        } catch (SQLException e) {
            e.printStackTrace();
            Utils.showAlertDialogWithoutCancel(this, "Error", "Error Occurred While Access Database");
            return;
        }
        //Refresh list if successful
        if (success) {
            Log.e(Globals.TAG, "Bid Has Been Placed For This Item!");
            refresh();
        } else {
            Utils.showAlertDialogWithoutCancel(this, "Error", "Unfortunately! An Error Occurred While Inserting Item In The Database. Please Try Again!");
        }
    }

    //Handles database manipulation and error handling for fetching auction items
    public void insertNewBidOnItem(int itemID, int amount, int userID, String username){

        boolean success;
        // Invoke Database's Insert New Auction Item method which attempts to insert new auction item in the database
        // and return an Item instance if successful
        try {
            database.open();
            success = database.addNewBidOnItem(amount, userID, username, itemID);
            database.close();
        } catch (SQLException e) {
            e.printStackTrace();
            Utils.showAlertDialogWithoutCancel(this, "Error", "Error Occurred While Access Database");
            return;
        }

        //Refresh list if successful
        if (success) {
            refresh();
            if(userID == app.getUserID())
                Log.e(Globals.TAG, "Your Bid!");
            else
                Log.e(Globals.TAG, "Bid Has Been Placed For This Item!");
        }else {
            Utils.showAlertDialogWithoutCancel(this, "Error", "Unfortunately! An Error Occurred While Inserting Item In The Database. Please Try Again!");
        }
    }

    //Returns all bids of current user from database
    public ArrayList<Bid> getMyBids() {
        ArrayList<Bid> allBids = null;
        try {
            database.open();
            allBids = database.getMyBids(app.getUserID());
            database.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return allBids;
    }

    //Invokes database's method to set the bid with given id winner on the item with given id
    public void setBidWinner(int bidID, int itemID) {
        int success = 0;
        try {
            database.open();
            success = database.setBidWinner(bidID, itemID);
            database.close();
        } catch (SQLException e) {
            e.printStackTrace();
            Utils.showAlertDialogWithoutCancel(this, "Error", "Error Occurred While Access Database");
            return;
        }

        if (success > 0) {
            //Refresh list
            refresh();
            // Prompt Registration success
            Toast.makeText(this, "Bid Has Been Accepted For This Item!", Toast.LENGTH_SHORT).show();
            Log.e(Globals.TAG, "Bid Has Been Accepted For This Item!");
        } else {
            Utils.showAlertDialogWithoutCancel(this, "Error", "Unfortunately! An Error Occurred While Inserting Item In The Database. Please Try Again!");
        }

    }

    @Override
    public void onAdded() {
        //Refresh list
        refresh();
    }
}
