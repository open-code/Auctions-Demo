package com.zuhaib.crossoverauctions.Bot;

import android.content.Context;
import android.widget.Toast;

import com.zuhaib.crossoverauctions.activities.ItemsListActivity;
import com.zuhaib.crossoverauctions.models.Item;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Zuhaib on 1/3/2016.
 */
public class AutoBiddingBot {

    private Random r;
    Context c;

    public AutoBiddingBot(Context c) {
        r = new Random();
        this.c = c;
    }


    public void start(){
        //Get Item List Activity instance from context
        ItemsListActivity activity = (ItemsListActivity) c;

        // Fetch Auction Items from Database
        ArrayList<Item> items = activity.getAuctionItemsFromDatabase();

        //Insert Random bids with Random Amount on Items
        for(Item i : items){
            //Place a bid only when random number is even
            if(r.nextInt() % 2 == 0) {
                activity.insertNewBidOnItem(i.getId(), r.nextInt(1000 - 50) + 50);
            }
        }

        // Notify
        Toast.makeText(c,"Bot Has Finished Bidding",Toast.LENGTH_SHORT).show();
        // Refresh List
        activity.addAuctionItemsToRecyclerView();
    }
}
