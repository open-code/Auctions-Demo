package com.zuhaib.crossoverauctions.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;

import com.zuhaib.crossoverauctions.models.Bid;
import com.zuhaib.crossoverauctions.models.Item;
import com.zuhaib.crossoverauctions.models.User;
import com.zuhaib.crossoverauctions.utils.Utils;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Zuhaib on 12/31/2015.
 */
public class AuctionsDatabase {

    //============================================================================================//
    //                                  References Type Variables                                 //
    //============================================================================================//
    private DatabaseHelper mDBHelper;
    private SQLiteDatabase mDatabase;
    private Context context;


    //============================================================================================//
    //                                  Database Specific Variables                               //
    //============================================================================================//
    //Database Version
    private static final int DATABASE_VERSION = 1;
    //Database Name
    private static final String DATABASE_NAME = "AUCTIONS_DATABASE";

    //Table Names
    public static final String TABLE_USERS = "users";
    public static final String TABLE_ITEMS = "items";
    public static final String TABLE_BIDS = "bids";

    // Common column names
    private static final String KEY_ID = "id";
    private static final String KEY_CREATED_AT = "created_at";



    //============================================================================================//
    //                                      Interfaces                                            //
    //============================================================================================//

    // USERS table columns
    private interface UsersColumns {
        String KEY_USERNAME = "username";
        String KEY_PASSWORD = "password";
        String KEY_EMAIL = "email";
        String KEY_DOB = "dateOfBirth";
    }

    // ITEMS table columns
    private interface ItemsColumns {
        String KEY_NAME = "name";
        String KEY_DESCRIPTION = "description";
        String KEY_IMAGE = "image";
        String KEY_STARTING_PRICE = "startingPrice";
        String KEY_OPEN_TIME = "openTime";
        String KEY_CLOSE_TIME = "closeTime";
        String KEY_DONOR_NAME = "donorName";
        String KEY_DONOR_ID = "donorID";
        String KEY_ACCEPTED_BID_ID = "acceptedBidID";
    }

    // BIDS table columns
    private interface BidsColumns {
        String KEY_AMOUNT = "amount";
        String KEY_ITEM_ID = "itemID";
        String KEY_BIDDER_ID = "bidderID";
        String KEY_BIDDER_NAME = "bidderName";
    }


    //============================================================================================//
    //                              Interface Carrying SQL Queries                                //
    //============================================================================================//
    private interface Queries {
        // Create User Table
        String CREATE_USER_TABLE_SQL = "CREATE TABLE " + TABLE_USERS + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + UsersColumns.KEY_USERNAME + " TEXT,"
                + UsersColumns.KEY_PASSWORD + " TEXT,"
                + UsersColumns.KEY_EMAIL + " TEXT,"
                + UsersColumns.KEY_DOB + " DATETIME,"
                + KEY_CREATED_AT + " DATETIME"
                + ")";

        // Create Items table
        String CREATE_ITEMS_TABLE_SQL = "CREATE TABLE " + TABLE_ITEMS + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + ItemsColumns.KEY_NAME + " TEXT,"
                + ItemsColumns.KEY_DESCRIPTION + " TEXT,"
                + ItemsColumns.KEY_IMAGE + " BLOB,"
                + ItemsColumns.KEY_STARTING_PRICE + " INTEGER,"
                + ItemsColumns.KEY_OPEN_TIME + " DATE,"
                + ItemsColumns.KEY_CLOSE_TIME + " DATE,"
                + ItemsColumns.KEY_DONOR_NAME + " TEXT,"
                + ItemsColumns.KEY_DONOR_ID + " INTEGER,"
                + ItemsColumns.KEY_ACCEPTED_BID_ID + " INTEGER,"
                + KEY_CREATED_AT + " DATETIME,"
                + "FOREIGN KEY("+ItemsColumns.KEY_DONOR_ID+") REFERENCES "+TABLE_USERS+"("+KEY_ID+"), "
                + "FOREIGN KEY("+ItemsColumns.KEY_DONOR_NAME+") REFERENCES "+TABLE_USERS+"("+UsersColumns.KEY_USERNAME+")"
                + "FOREIGN KEY("+ItemsColumns.KEY_ACCEPTED_BID_ID+") REFERENCES "+TABLE_BIDS+"("+KEY_ID+")"
                + ")";

        // Create Bids table
        String CREATE_BIDS_TABLE_SQL = "CREATE TABLE " + TABLE_BIDS + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + BidsColumns.KEY_AMOUNT + " INTEGER,"
                + BidsColumns.KEY_BIDDER_ID + " INTEGER,"
                + BidsColumns.KEY_BIDDER_NAME + " TEXT,"
                + BidsColumns.KEY_ITEM_ID + " INTEGER,"
                + KEY_CREATED_AT + " DATETIME,"
                + "FOREIGN KEY("+BidsColumns.KEY_BIDDER_ID+") REFERENCES "+TABLE_USERS+"("+KEY_ID+"), "
                + "FOREIGN KEY("+BidsColumns.KEY_BIDDER_NAME+") REFERENCES "+TABLE_USERS+"("+UsersColumns.KEY_USERNAME+")"
                + "FOREIGN KEY("+BidsColumns.KEY_ITEM_ID+") REFERENCES "+TABLE_ITEMS+"("+KEY_ID+")"
                + ")";

        //Get all items from Items table
        String GET_ALL_AUCTION_ITEMS_SQL = "SELECT DISTINCT * FROM " + TABLE_ITEMS;

        //TODO Change queries
        //Get all items won from Items table
        String GET_WON_AUCTION_ITEMS_SQL = "SELECT DISTINCT * FROM " + TABLE_ITEMS
                                            + " LEFT OUTER JOIN " + TABLE_BIDS
                                            + " ON " + TABLE_ITEMS + "." + KEY_ID + " = " + TABLE_BIDS + "." + BidsColumns.KEY_ITEM_ID
                                            + " WHERE " + ItemsColumns.KEY_ACCEPTED_BID_ID + " = " + TABLE_BIDS + "." + KEY_ID
                                            + " AND " + TABLE_BIDS + "." +BidsColumns.KEY_BIDDER_ID + " =?";

        //Get all Bidding items from Items table
        String GET_BIDDING_AUCTION_ITEMS_SQL = "SELECT DISTINCT * FROM " + TABLE_ITEMS
                                            + " LEFT OUTER JOIN " + TABLE_BIDS
                                            + " ON " + TABLE_ITEMS + "." + KEY_ID + " = " + TABLE_BIDS + "." + BidsColumns.KEY_ITEM_ID
                                            + " WHERE " + BidsColumns.KEY_BIDDER_ID + " =?";

        //Get all Bidding items from Items table
        String GET_NUMBER_OF_BIDS_SQL = "SELECT * FROM " + TABLE_BIDS + " WHERE " + BidsColumns.KEY_ITEM_ID + " = ";

        //Get all bids having my ID
        String GET_MY_BIDS = "SELECT * FROM " + TABLE_BIDS + " WHERE " + BidsColumns.KEY_BIDDER_ID + " =?";
    }


    //============================================================================================//
    //                                  Database Helper Class                                     //
    //============================================================================================//
    private class DatabaseHelper extends SQLiteOpenHelper {

        public DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            //Create User, Item and Bid tables
            db.execSQL(Queries.CREATE_USER_TABLE_SQL);
            db.execSQL(Queries.CREATE_ITEMS_TABLE_SQL);
            db.execSQL(Queries.CREATE_BIDS_TABLE_SQL);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_ITEMS);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_BIDS);
            onCreate(db);
        }
    }

    // Database Constructor
    public AuctionsDatabase(Context c) {
        this.context = c;
    }

    // Open Database in Writable mode
    public AuctionsDatabase open() throws SQLException {
        mDBHelper = new DatabaseHelper(this.context);
        mDatabase = mDBHelper.getWritableDatabase();
        return this;
    }

    // Close Database
    public void close() {
        if (mDBHelper != null) {
            mDBHelper.close();
        }
    }

    //============================================================================================//
    //                              User Created Database Operations                              //
    //============================================================================================//

    //Attempt to login a user
    public User attemptUserLogin(String username, String password) {
        Cursor c = mDatabase.rawQuery("SELECT * FROM " + TABLE_USERS +
                " WHERE " + UsersColumns.KEY_USERNAME + " = \"" + username + "\" AND " + UsersColumns.KEY_PASSWORD + " = \"" + password + "\"", null);
        c.moveToFirst();

        User u = null;
        if (c.getCount() > 0) {
            u = new User(
                    c.getInt(c.getColumnIndex(KEY_ID)),
                    c.getString(c.getColumnIndex(UsersColumns.KEY_USERNAME)),
                    c.getString(c.getColumnIndex(UsersColumns.KEY_PASSWORD)),
                    c.getString(c.getColumnIndex(UsersColumns.KEY_EMAIL)),
                    c.getString(c.getColumnIndex(UsersColumns.KEY_DOB)),
                    c.getString(c.getColumnIndex(KEY_CREATED_AT))
            );
        }
        return u;
    }

    // Attempt to register new user by inserting new user in database
    public boolean attemptUserRegister(String username, String email, String password, String dob) {
        ContentValues cv = new ContentValues();
        cv.put(UsersColumns.KEY_USERNAME, username);
        cv.put(UsersColumns.KEY_PASSWORD, password);
        cv.put(UsersColumns.KEY_EMAIL, email);
        cv.put(UsersColumns.KEY_DOB, dob);
        cv.put(KEY_CREATED_AT, Utils.getDateTime());
        long success = mDatabase.insert(TABLE_USERS, null, cv);
        if (success != -1) return true;
        return false;
    }

    // Get all auction items from database
    public ArrayList<Item> getAllAuctionItems() {
        ArrayList<Item> allAuctionItems = new ArrayList<>();
        Cursor c = mDatabase.rawQuery(Queries.GET_ALL_AUCTION_ITEMS_SQL, null);
        c.moveToFirst();
        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
            allAuctionItems.add(new Item(
                            c.getInt(c.getColumnIndex(KEY_ID)),
                            c.getInt(c.getColumnIndex(ItemsColumns.KEY_DONOR_ID)),
                            c.getString(c.getColumnIndex(ItemsColumns.KEY_NAME)),
                            c.getString(c.getColumnIndex(ItemsColumns.KEY_DESCRIPTION)),
                            c.getString(c.getColumnIndex(ItemsColumns.KEY_DONOR_NAME)),
                            Utils.blobToBitmap(c.getBlob(c.getColumnIndex(ItemsColumns.KEY_IMAGE))),
                            c.getInt(c.getColumnIndex(ItemsColumns.KEY_STARTING_PRICE)),
                            c.getString(c.getColumnIndex(ItemsColumns.KEY_OPEN_TIME)),
                            c.getString(c.getColumnIndex(ItemsColumns.KEY_CLOSE_TIME)),
                            c.getInt(c.getColumnIndex(ItemsColumns.KEY_ACCEPTED_BID_ID))
                    )
            );
        }
        return allAuctionItems;
    }

    // Get all items on which user with given id has bidded
    public ArrayList<Item> getAllItemsBidding(int userID) {
        ArrayList<Item> allAuctionItems = new ArrayList<>();
        Cursor c = mDatabase.rawQuery(Queries.GET_BIDDING_AUCTION_ITEMS_SQL, new String[]{String.valueOf(userID)});
        c.moveToFirst();
        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
            allAuctionItems.add(new Item(
                            c.getInt(c.getColumnIndex(KEY_ID)),
                            c.getInt(c.getColumnIndex(ItemsColumns.KEY_DONOR_ID)),
                            c.getString(c.getColumnIndex(ItemsColumns.KEY_NAME)),
                            c.getString(c.getColumnIndex(ItemsColumns.KEY_DESCRIPTION)),
                            c.getString(c.getColumnIndex(ItemsColumns.KEY_DONOR_NAME)),
                            Utils.blobToBitmap(c.getBlob(c.getColumnIndex(ItemsColumns.KEY_IMAGE))),
                            c.getInt(c.getColumnIndex(ItemsColumns.KEY_STARTING_PRICE)),
                            c.getString(c.getColumnIndex(ItemsColumns.KEY_OPEN_TIME)),
                            c.getString(c.getColumnIndex(ItemsColumns.KEY_CLOSE_TIME)),
                            c.getInt(c.getColumnIndex(ItemsColumns.KEY_ACCEPTED_BID_ID))
                    )
            );
        }
        return allAuctionItems;
    }

    // Get all items won by the user having given id
    public ArrayList<Item> getAllItemsWon(int userID) {
        ArrayList<Item> allAuctionItems = new ArrayList<>();
        Cursor c = mDatabase.rawQuery(Queries.GET_WON_AUCTION_ITEMS_SQL,new String[]{String.valueOf(userID)});
        c.moveToFirst();
        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
            allAuctionItems.add(new Item(
                            c.getInt(c.getColumnIndex(KEY_ID)),
                            c.getInt(c.getColumnIndex(ItemsColumns.KEY_DONOR_ID)),
                            c.getString(c.getColumnIndex(ItemsColumns.KEY_NAME)),
                            c.getString(c.getColumnIndex(ItemsColumns.KEY_DESCRIPTION)),
                            c.getString(c.getColumnIndex(ItemsColumns.KEY_DONOR_NAME)),
                            Utils.blobToBitmap(c.getBlob(c.getColumnIndex(ItemsColumns.KEY_IMAGE))),
                            c.getInt(c.getColumnIndex(ItemsColumns.KEY_STARTING_PRICE)),
                            c.getString(c.getColumnIndex(ItemsColumns.KEY_OPEN_TIME)),
                            c.getString(c.getColumnIndex(ItemsColumns.KEY_CLOSE_TIME)),
                            c.getInt(c.getColumnIndex(ItemsColumns.KEY_ACCEPTED_BID_ID))
                    )
            );
        }
        return allAuctionItems;
    }


    // Insert new Auction item to database
    public boolean addNewAuctionItem(int donorID, String name, String description, String donorName, Bitmap image, int price,
                                     String openTime, String closeTime) {
        ContentValues cv = new ContentValues();
        cv.put(ItemsColumns.KEY_DONOR_ID, donorID);
        cv.put(ItemsColumns.KEY_NAME, name);
        cv.put(ItemsColumns.KEY_DESCRIPTION, description);
        cv.put(ItemsColumns.KEY_DONOR_NAME, donorName);
        cv.put(ItemsColumns.KEY_IMAGE, Utils.bitmapToBlob(image));
        cv.put(ItemsColumns.KEY_STARTING_PRICE, price);
        cv.put(ItemsColumns.KEY_OPEN_TIME, openTime);
        cv.put(ItemsColumns.KEY_CLOSE_TIME, closeTime);
        cv.put(KEY_CREATED_AT, Utils.getDateTime());
        long success = mDatabase.insert(TABLE_ITEMS, null, cv);
        if (success != -1) return true;
        return false;
    }

    // Place a new bid on the provided item
    public boolean addNewBidOnItem(int amount, int bidderID, String bidderName, int itemID) {
        ContentValues cv = new ContentValues();
        cv.put(BidsColumns.KEY_AMOUNT, amount);
        cv.put(BidsColumns.KEY_BIDDER_ID, bidderID);
        cv.put(BidsColumns.KEY_BIDDER_NAME, bidderName);
        cv.put(BidsColumns.KEY_ITEM_ID, itemID);
        cv.put(KEY_CREATED_AT, Utils.getDateTime());
        long success = mDatabase.insert(TABLE_BIDS, null, cv);
        if (success != -1) return true;
        return false;
    }

    // Get number of bids on item with given id
    public int getNumberOfBids(int id) {
        Cursor c = mDatabase.rawQuery(Queries.GET_NUMBER_OF_BIDS_SQL + id, null);
        c.moveToFirst();
        return c.getCount();
    }

    //Get all bids made by this user
    public ArrayList<Bid> getMyBids(int userID) {
        ArrayList<Bid> bids = new ArrayList<>();
        Cursor c = mDatabase.rawQuery(Queries.GET_MY_BIDS, new String[]{String.valueOf(userID)});
        c.moveToFirst();
        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
            bids.add(new Bid(
                            c.getInt(c.getColumnIndex(KEY_ID)),
                            c.getInt(c.getColumnIndex(BidsColumns.KEY_AMOUNT)),
                            c.getString(c.getColumnIndex(BidsColumns.KEY_BIDDER_NAME)),
                            c.getInt(c.getColumnIndex(BidsColumns.KEY_ITEM_ID)),
                            c.getInt(c.getColumnIndex(BidsColumns.KEY_BIDDER_ID))
                    )
            );
        }
        return bids;
    }


    //Make the Bid with provided bidID win on the Auction of Item having given itemID
    public int setBidWinner(int bidID, int itemID) {
        ContentValues cv = new ContentValues();
        cv.put(ItemsColumns.KEY_ACCEPTED_BID_ID, bidID);
        return mDatabase.update(TABLE_ITEMS,cv,KEY_ID+"=?",new String[]{String.valueOf(itemID)});
    }
}
